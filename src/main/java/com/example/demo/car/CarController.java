package com.example.demo.car;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("cars")
public class CarController {

  @GetMapping
  public ResponseEntity<List<Car>> getAllCars() {
    List<Car> cars = new ArrayList<>();
    cars.add(new Car("VW", "Gol", 2015));
    cars.add(new Car("VW", "Jetta", 2019));
    cars.add(new Car("LADA", "Laica", 1987));
    return ResponseEntity.ok().body(cars);
  }
}
