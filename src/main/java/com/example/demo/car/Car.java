package com.example.demo.car;

public record Car(String manufacturer, String model, Integer year) {

}
